#!/bin/bash
echo Tag?
read tag

docker buildx build --platform linux/amd64 -t registry.gitlab.com/packagemoverr/jyb/jyb-mariadb-remote-backup:${tag:-'latest'} .
docker push registry.gitlab.com/packagemoverr/jyb/jyb-mariadb-remote-backup:${tag:-'latest'}