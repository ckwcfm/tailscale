FROM debian:bullseye
LABEL maintainer kevinwong@gmail.com

RUN apt update
RUN apt install curl vim openssh-server mariadb-client cron -y 
RUN curl -fsSL https://tailscale.com/install.sh | sh
WORKDIR /
COPY ./setup.sh ./setup.sh
COPY ./cron.sh ./cron.sh
COPY ./ssh/ ./root/.ssh/
RUN chmod +x ./setup.sh

CMD [ "./setup.sh" ]

