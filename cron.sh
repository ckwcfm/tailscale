#!/bin/bash

DB_HOST=`printenv db_host`
DB_USER=`printenv db_user`
DB_PASSWORD=`printenv db_password`
SSH_HOST=`printenv SSH_HOST`
SSH_PORT=`printenv SSH_PORT`
DB_NAME=`printenv DB_NAME`

YEAR=$(date +%Y)
MONTH=$(date +%m)
DAY=$(date +%d)
DIR=${DB_NAME}_${YEAR}${MONTH}${DAY}.gz

/usr/bin/mysqldump -u ${DB_USER} --password=${DB_PASSWORD} -h ${DB_HOST} --verbose=false ${DB_NAME} | gzip > ${DIR} 
/usr/bin/ssh scp -P ${SSH_PORT:-'22'} -o StrictHostKeyChecking=no ${DIR} ${SSH_HOST}
rm -rf ${DIR}
