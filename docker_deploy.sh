#!/bin/bash
echo Tag?
read tag

docker buildx build --platform linux/amd64 -t registry.gitlab.com/ckwcfm/tailscale:${tag:-'latest'} .
docker push registry.gitlab.com/ckwcfm/tailscale:${tag:-'latest'}